package com.junior.notesmaker;

import android.support.annotation.NonNull;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

/**
 * Created by Алексей on 27.10.2018.
 */

public class TipsPageAdapter extends FragmentPagerAdapter {


    public TipsPageAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new FirstTip();
            case 1:
                return new SecondTip();
            case 2:
                return new LastTip();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
