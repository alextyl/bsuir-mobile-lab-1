package com.junior.notesmaker.realm;


import com.junior.notesmaker.realm.realmObjects.Note;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmProvider {

    private static final RealmProvider ourInstance = new RealmProvider();

    public static RealmProvider getInstance() {
        return ourInstance;
    }

    private RealmProvider() {}

    public Realm getRealm(){
       return Realm.getDefaultInstance();
    }

    public void setNote(final Date date, final String description, final String title, final boolean important){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Note note = realm.createObject(Note.class, date.getTime());
                note.setDate(date);
                note.setDescription(description);
                note.setTitle(title);
                note.setImportant(important);
            }
        });
    }

    public void deleteNote(final Date date){
            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Note note = getRealm().where(Note.class).equalTo("id", date.getTime()).findFirst();
                    if(note != null)
                        note.deleteFromRealm();
                }
            });
    }

    public RealmResults<Note> getNotesRealm(){
        return getRealm().where(Note.class).sort("id", Sort.DESCENDING).findAll();
    }


    public RealmResults<Note> getNotesRealm(String s){
        if(s.trim().equals(""))
            return getNotesRealm();
        return getRealm().where(Note.class).contains("description", s).or().contains("title", s).sort("id", Sort.DESCENDING).findAll();
    }


    public ArrayList<Note> getNotes(){
        RealmResults<Note> results = getNotesRealm();
        ArrayList<Note> notes = new ArrayList<>();
        notes.addAll(results);
        return notes;
    }

    public ArrayList<Note> getNotes(String s){
        RealmResults<Note> results = getNotesRealm(s);
        ArrayList<Note> notes = new ArrayList<>();
        notes.addAll(results);
        return notes;
    }

}
