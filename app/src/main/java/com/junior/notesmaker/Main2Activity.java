package com.junior.notesmaker;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        getSupportActionBar().hide();

        App.isShowTips = true;

        TipsPageAdapter tipsPageAdapter = new TipsPageAdapter(getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.viewpager);

        viewPager.setAdapter(tipsPageAdapter);


    }

    @Override
    public void onBackPressed() {

    }
}
