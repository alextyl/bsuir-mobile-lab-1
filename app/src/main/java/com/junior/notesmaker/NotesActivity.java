package com.junior.notesmaker;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import com.junior.notesmaker.realm.RealmProvider;
import com.junior.notesmaker.realm.realmObjects.Note;

import java.util.ArrayList;
import java.util.Date;

public class NotesActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private MenuItem menuItem;
    private SearchView searchView;
    private NotesAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!App.isShowTips){
            Intent intent = new Intent(this, Main2Activity.class);
            startActivity(intent);
        }

        setContentView(R.layout.activity_notes);

        recyclerView = findViewById(R.id.recycler_notes);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NotesAdapter(RealmProvider.getInstance().getNotes());
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                new LinearLayoutManager(this).getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        fab = findViewById(R.id.floating_action_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog().show();
            }
        });


    }

    @Override
    public void onBackPressed() {

    }



    private void addNote(String description, String title, boolean important){
        RealmProvider.getInstance().setNote(new Date(), description, title, important);
        adapter.setNotes(RealmProvider.getInstance().getNotes());
        adapter.notifyDataSetChanged();
        Toast.makeText(NotesActivity.this, R.string.add_note, Toast.LENGTH_SHORT).show();
    }

    private Dialog createDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog, null);

        final EditText title = view.findViewById(R.id.editText);
        final EditText description = view.findViewById(R.id.editText2);
        final SwitchCompat switcher = view.findViewById(R.id.switch1);

        builder.setView(view)
                .setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        addNote(description.getText().toString(), title.getText().toString(), switcher.isChecked());
                    }
                })
                .setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return  builder.create();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        menuItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Search");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.setNotes(RealmProvider.getInstance().getNotes(newText));
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesHolder>{

        private ArrayList<Note> notes;

        public NotesAdapter(ArrayList<Note> notes){
            this.notes = notes;
        }

        public void setNotes(ArrayList<Note> notes){
            this.notes = notes;
            notifyDataSetChanged();
        }

        public Note getNote(int position){
            return notes.get(position);
        }

        @Override
        public NotesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new NotesHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull NotesHolder holder, int position) {
            holder.date.setText(App.dateFormat.format(notes.get(position).getDate()));
            holder.description.setText(notes.get(position).getDescription());
            holder.title.setText(notes.get(position).getTitle());
            holder.view.setBackgroundColor(notes.get(position).getImportant() ? ContextCompat.getColor(NotesActivity.this, R.color.Important) : Color.WHITE);
        }

        @Override
        public int getItemCount() {
            return notes.size();
        }

        class NotesHolder extends RecyclerView.ViewHolder{

            TextView date;
            TextView description;
            TextView title;
            View view;

            NotesHolder(View itemView) {
                super(itemView);
                view = itemView;
                date = itemView.findViewById(R.id.date);
                description = itemView.findViewById(R.id.description);
                title = itemView.findViewById(R.id.title);
            }
        }

    }

    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            final int position = viewHolder.getAdapterPosition();
            RealmProvider.getInstance().deleteNote(adapter.getNote(position).getDate());
            adapter.setNotes(RealmProvider.getInstance().getNotes());
        }
    };
}
